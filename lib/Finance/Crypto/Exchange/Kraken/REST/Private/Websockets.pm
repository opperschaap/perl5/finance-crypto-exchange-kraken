package Finance::Crypto::Exchange::Kraken::REST::Private::Websockets;
our $VERSION = '0.005';
use Moose::Role;

# ABSTRACT: Finance::Crypto::Exchange::Kraken::REST::Private::Websockets needs an abstract

requires qw(
    _private
);

sub get_websockets_token {
    my $self = shift;
    my $req = $self->_private('GetWebSocketsToken', @_);
    return $self->call($req);
}

1;

__END__

=head1 DESCRIPTION

This role introduces the REST API for websockets Kraken supports. For extensive
information please have a look at the L<Kraken API
manual|https://www.kraken.com/features/api#ws-auth>

=head1 SYNOPSIS

    package Foo;

    use Moose;
    with qw(Finance::Crypto::Exchange::Kraken::REST::Private::Websockets);

=head1 METHODS

=head2 get_websockets_token

L<https://api.kraken.com/0/private/GetWebsocketsToken>
